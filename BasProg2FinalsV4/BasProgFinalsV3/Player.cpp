#include "Player.h"
#include "Pokemon.h"
//Creating the player and assigning them a starter pokemon
Player::Player(string name, vector<Pokemon*> pokemon)
{
	Pokemon starterPokemon;
	this->name = name;
	cout << "Choose your Starter Pokemon: " << endl;
	for (int i = 0; i < 4; i++)
	{
		cout << i + 1 << ": " << pokemon[i]->name << endl;
	}
	cout << "Who do you choose? ";
	int choice;
	cin >> choice;
	switch (choice)
	{
	case 1:
		starterPokemon = *pokemon[0];
		break;
	case 2:
		starterPokemon = *pokemon[1];
		break;
	case 3:
		starterPokemon = *pokemon[2];
		break;
	case 4:
		starterPokemon = *pokemon[3];
		break;
	}
	for (int i = 0; i < 4; i++) // Level starter Pokemon to lvl 5
	{
		starterPokemon.lvlUp();
	}
	this->addPokemon(starterPokemon);
	cout << "You chose " << this->party[0].name << " as your partner!" << endl;
	system("pause");
	system("CLS");
}
//Can add selected pokemon until the limit of 6
void Player::addPokemon(Pokemon newPokemon)
{
	if (this->party.size() < 6)
	{
		this->party.push_back(newPokemon);
	}
	else
	{
		cout << "Your party is full, you can't add this pokemon" << endl;
	}
}
//Find the next available pokemon for battle
int Player::findAvaliablePokemon()
{
	int position = 0;
	while (position < this->party.size())
	{
		if (this->party[position].hp <= 0)
		{
			position++;
		}
		else
		{
			break;
		}
	}
	return position;
}
//Determines if player is in wild zone
void Player::checkAreaSafe()
{
	if (this->area == "Pallet Town" || this->area == "Viridian City"||this->area == "Pewter City" || this->area == "Cerulean City") // All safe areas
	{
		this->isInSafeArea = true;
	}
	else // Will cover all non safe areas
	{
		this->isInSafeArea = false;
	}
}
//Restoring Player's pokemon health
void Player::pokeCenter()
{
	cout << "Welcome to the " << this->area << " PokeCenter!" << endl;
	cout << "Would you like me to heal your Pokemon? y/n" << endl;
	string choice;
	cin >> choice;
	if (choice == "y" || choice == "Y")
	{
		for (int i = 0; i < this->party.size(); i++) // Repeat processes for how many pokemon is in team
		{
			this->party[i].hp = this->party[i].baseHp;
		}
		cout << "Your pokemon are brought back to full health" << endl;
	}
	cout << "We hope you to see you again!" << endl;
}
//Moves player
void Player::moveTo(int moveX, int moveY)
{
	this->x += moveX;
	this->y += moveY;
}
//Determines the local pokemon of specific areas
int Player::determineAreaPokemon()
{
	/*pokemon = { pikachu, bulbasaur ,charmander, squirtle, chikorita, cyndaquill,
		totodile, treecko, torchic, mudkip, turtwig, chimchar, piplup, snivy, tepig, oshawott};*/
	int min = 0, max = 0;
	if (this->area == "Route 1") //Bulbasaur, Charmander, Squirtle
	{
		min = 1; max = 3;
	}
	else if (this->area == "Route 2") //Chikorita, Cybdaquill, Totodile
	{
		min = 4; max = 6;
	}
	else if (this->area == "Viridian Forest") //Pikachu
	{
		min = 0; max = 0;
	}
	else if (this->area == "Route 3") //Treecko, Torchicm, Mudkip
	{
		min = 7; max = 9;
	}
	else if (this->area == "Mt. Moon") //Turtwig, Chimchar, Piplup
	{
		min = 10; max = 12;
	}
	else if (this->area == "Route 4") //Snivy, Tepig, Oshawott
	{
		min = 13; max = 15;
	}
	return min + (rand() % (max - min + 1)); //Returns a random value between the range selected
}
//Determines what pokeball is thrown
void Player::throwPokeball(int pokeBallChoice, int catchChance, bool &pokeBallThrown, bool &pokemonCaught)
{
	pokeBallThrown = false;
	switch (pokeBallChoice)
	{
	case 1:
		if (this->pokeBalls > 0) //PokeBall
		{
			this->pokeBalls--;
			cout << "You throw a PokeBall!" << endl;
			pokeBallThrown = true;
			if (catchChance >= rand() % 100 + 1) //Pokemon Caught
			{
				pokemonCaught = true;
			}
		}
		break;
	case 2:
		if (this->greatBalls > 0) //GreatBall
		{
			this->greatBalls--;
			cout << "You throw a GreatBall!" << endl;
			pokeBallThrown = true;
			if (catchChance * 1.5 >= rand() % 100 + 1) //Pokemon Caught
			{
				pokemonCaught = true;
			}
		}
		break;
	case 3:
		if (this->ultraBalls > 0) //UltraBall
		{
			this->ultraBalls--;
			cout << "You throw a UltraBall!" << endl;
			pokeBallThrown = true;
			if (catchChance * 2 >= rand() % 100 + 1) //Pokemon Caught
			{
				pokemonCaught = true;
			}
		}
		break;
	default:
		cout << "Please choose one of the options" << endl;
		break;
	}
}
//Gets the result of the PokeBall thrown
void Player::pokeBallThrowResult(bool pokemonCaught, Pokemon wildPokemon, bool &isBattleFinished, bool pokeBallThrown)
{
	if (pokemonCaught == true) //Pokemon Caught
	{
		cout << wildPokemon.name << " Caught!" << endl;
		cout << wildPokemon.name << " is now your friend!" << endl;
		isBattleFinished = true;
	}
	else if (pokeBallThrown == true)  //Pokemon Escaped
	{
		cout << wildPokemon.name << " got out of the PokeBall!" << endl;
	}
	else //No Pokeballs avaliable of that type
	{
		cout << "You dont have any PokeBalls of that type" << endl;
	}
}

//Runs a Pokemon Battle if triggered
void Player::pokemonBattle(vector<Pokemon*> pokemon)
{
	//Battle Setup
	int choice, pokeBallChoice, catchChance = 30, wildLvl = rand() % 10 + 1;
	int pokemonPosition = this->determineAreaPokemon();
	bool isBattleFinished = false, pokemonCaught = false, isPokemonOnField = false, pokeBallThrown = false;
	Pokemon wildPokemon = *pokemon[pokemonPosition];
	for (int i = 0; i < wildLvl - 1; i++) //Lvl Up wild Pokemon before battle
	{
		wildPokemon.lvlUp();
	}
	pokemonPosition = this->findAvaliablePokemon();
	Pokemon* playerPokemon = NULL;
	cout << "A wild " << wildPokemon.name << " appears!" << endl << endl;
	if (pokemonPosition >= this->party.size()) //If all pokemon have fainted before battle
	{
		cout << "All your pokemon are too tired to battle, please go back to the nearest PokeCenter" << endl;
		isBattleFinished = true;
	}
	//Battle Loop
	while (isBattleFinished == false)
	{
		system("pause");
		system("CLS");
		if (isPokemonOnField == false) //Sets the playerPokemon
		{
			playerPokemon = &this->party[pokemonPosition];
			cout << "You bring out " << playerPokemon->name << endl << endl;
			isPokemonOnField = true;
		}
		wildPokemon.displayStats();
		cout << "1. Attack  2. Catch  3. Runaway" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1: //Battle
			playerPokemon->dealDamage(wildPokemon);
			if (wildPokemon.hp > 0) //Wild Pokemon won't attack if hp = 0
			{
				wildPokemon.dealDamage(*playerPokemon);
			}
			break;
		case 2: //Catch
			cout << "Choose what PokeBall you will use" << endl;
			cout << "1. PokeBall(" << this->pokeBalls << ")  2. GreatBalls(" << this->greatBalls << ")  3. UltraBalls(" << this->ultraBalls << ")" << endl;
			cin >> pokeBallChoice;
			throwPokeball(pokeBallChoice, catchChance, pokeBallThrown, pokemonCaught); //Throw Pokeball
			pokeBallThrowResult(pokemonCaught, wildPokemon, isBattleFinished, pokeBallThrown); //Get Result of Pokeball thrown
			break;
		case 3: //Run
			cout << this->name << " ran away..." << endl;
			isBattleFinished = true;
			break;
		default:
			cout << "Please choose one of the options" << endl;
			break;
		}
		if (playerPokemon->hp <= 0)//If current player pokemon has no hp
		{
			cout << playerPokemon->name << " fainted..." << endl;
			pokemonPosition++;
			isPokemonOnField = false;
			if (pokemonPosition >= this->party.size()) //If all pokemon in party have fainted
			{
				cout << "All your pokemon have fainted, please go back to the nearest PokeCenter" << endl;
				isBattleFinished = true;
			}
		}
		else if (wildPokemon.hp <= 0)//If wild Pokemon has no hp
		{
			cout << playerPokemon->name << " Beat the wild " << wildPokemon.name << "!" << endl;
			cout << playerPokemon->name << " got " << wildPokemon.expYield << " exp!" << endl;
			playerPokemon->exp += wildPokemon.expYield;
			cout << "The wild Pokemon dropped some cash!" << endl;
			cout << "You pick up the P" << wildPokemon.moneyYield << endl;
			this->pokeDollars += wildPokemon.moneyYield;
			if (playerPokemon->exp >= playerPokemon->expToNextLvl) //Pokemon Lvl Up
			{
				playerPokemon->lvlUp();
				cout << playerPokemon->name << " Leveled up to " << playerPokemon->lvl << "!" << endl;
			}
			isBattleFinished = true;
		}
		else if (pokemonCaught == true) //Adding to team is done at the end due to pushback changing the memory address of elements
		{
			this->addPokemon(wildPokemon);
		}
	}
}
//Pokemon Shop
void Player::pokeMart()
{
	int choice;
	bool playerWillLeave = false;
	while (playerWillLeave == false) //Repeats unil player decides to leave
	{
		cout << "Welcome to the " << this->area << " PokeMart!" << endl;
		cout << "What would you like to buy?" << endl;
		cout << "1. PokeBall - P200  2. GreatBall - P600  3.UltraBall - P1200  4. Leave PokeMart" << endl;
		cout << "Your PokeDollars: P" << this->pokeDollars << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			if (this->pokeDollars >= 200)
			{
				this->pokeDollars -= 200;
				this->pokeBalls++;
				cout << "You got a PokeBall" << endl;
			}
			else
			{
				cout << "You don't have enough PokeDollars" << endl;
			}
			break;
		case 2:
			if (this->pokeDollars >= 600)
			{
				this->pokeDollars -= 600;
				this->greatBalls++;
				cout << "You got a GreatBall" << endl;
			}
			else
			{
				cout << "You don't have enough PokeDollars" << endl;
			}
			break;
		case 3:
			if (this->pokeDollars >= 1200)
			{
				this->pokeDollars -= 1200;
				cout << "You got a UltraBall" << endl;
				this->ultraBalls++;
			}
			else
			{
				cout << "You don't have enough PokeDollars" << endl;
			}
			break;
		case 4:
			playerWillLeave = true;
			break;
		}
		if (playerWillLeave == false)
		{
			system("pause");
			system("CLS");
		}
	}
}