#pragma once
#include<iostream>
#include<string>
#include<vector>
#include"Pokemon.h"

using namespace std;

class Player {
public:
	string name, area;
	int x = 0, y = 0, pokeDollars = 5000, pokeBalls = 5, greatBalls = 0, ultraBalls = 0;
	vector<Pokemon> party; // Not pointers, because chaning party pokemon stats will change the Pokedex standard
	bool isInSafeArea;
	Player(string name, vector<Pokemon*> pokemon);
	void addPokemon(Pokemon newPokemon);
	int findAvaliablePokemon();
	void checkAreaSafe();
	void pokeCenter();
	void moveTo(int moveX, int moveY);
	int determineAreaPokemon();
	void throwPokeball(int pokeBallChoice, int catchChance, bool& pokeBallThrown, bool& pokemonCaught);
	void pokeBallThrowResult(bool pokemonCaught, Pokemon wildPokemon, bool& isBattleFinished, bool pokeBallThrown);
	void pokemonBattle(vector<Pokemon*> pokemon);
	void pokeMart();
};