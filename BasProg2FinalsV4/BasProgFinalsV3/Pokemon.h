#pragma once
#include<iostream>
#include<string>

using namespace std;

class Pokemon {
public:
	string name, type;
	int baseHp, hp, lvl = 1, baseDamage, exp = 0, expToNextLvl, expYield, moneyYield;
	Pokemon();
	Pokemon(string name, string type, int baseHp, int baseDamage, int expToNextLvl, int expYield);
	double checkEffectiveness(Pokemon enemyPokemon);
	void dealDamage(Pokemon &enemyPokemon);
	void lvlUp();
	void displayStats();
};