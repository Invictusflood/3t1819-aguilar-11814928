#include "Pokemon.h"
//Pokemon Initializaiton
Pokemon::Pokemon()
{
	this->name = "";
	this->baseDamage = 0;
}
//Pokemon Creation
Pokemon::Pokemon(string name, string type, int baseHp, int baseDamage, int expToNextLvl, int expYield)
{
	this->name = name;
	this->type = type;
	this->baseHp = baseHp;
	this->hp = this->baseHp;
	this->baseDamage = baseDamage;
	this->expToNextLvl = expToNextLvl;
	this->expYield = expYield;
	this->moneyYield = 400;
}
//Pokemon type effectiveness check
double Pokemon::checkEffectiveness(Pokemon enemyPokemon)
{
	double multiplier = 1;
	if (this->type == "Grass")
	{
		if (enemyPokemon.type == "Water")
		{
			multiplier = 2;
		}
		else if (enemyPokemon.type == "Fire" || enemyPokemon.type == "Grass")
		{
			multiplier = 0.5;
		}
	}
	else if (this->type == "Fire")
	{
		if (enemyPokemon.type == "Grass")
		{
			multiplier = 2;
		}
		else if (enemyPokemon.type == "Water" || enemyPokemon.type == "Fire")
		{
			multiplier = 0.5;
		}
	}
	else if (this->type == "Water")
	{
		if (enemyPokemon.type == "Fire")
		{
			multiplier = 2;
		}
		else if (enemyPokemon.type == "Grass" || enemyPokemon.type == "Water")
		{
			multiplier = 0.5;
		}
	}
	else if (this->type == "Electric")
	{
		if (enemyPokemon.type == "Water")
		{
			multiplier = 2;
		}
		else if (enemyPokemon.type == "Grass" || enemyPokemon.type == "Electric")
		{
			multiplier = 0.5;
		}
	}
	return multiplier;
}
//Deal damage to enemy pokemon
void Pokemon::dealDamage(Pokemon &enemyPokemon)
{
	int attackChance = 8;
	if (attackChance >= rand() % 10 + 1) //Attack
	{
		double multiplier = checkEffectiveness(enemyPokemon);
		cout << this->name << " attacks " << enemyPokemon.name << " for " << this->baseDamage*multiplier << endl;
		if (multiplier == 2)
		{
			cout << "It was super effective!" << endl;
		}
		else if (multiplier == 0.5)
		{
			cout << "It wasn't very effective..." << endl;
		}
		enemyPokemon.hp -= this->baseDamage * multiplier;
		cout << enemyPokemon.name << " has " << enemyPokemon.hp << " hp" << endl;
	}
	else //Missed
	{
		cout << this->name << " missed..." << endl;
	}
	cout << endl;
}
//Pokemon Lvl Up
void Pokemon::lvlUp() //Loops for wild pokemon (Lvl Up before battle)
{
	this->lvl++;
	this->baseHp *= 1.15;
	this->hp *= 1.15;
	this->baseDamage *= 1.1;
	if (this->exp >= this->expToNextLvl) //Will trigger only if pokemon leveled due to their exp
	{
		this->exp -= this->expToNextLvl;
	}
	this->expToNextLvl *= 1.2;
	this->expYield *= 1.2;
	this->moneyYield *= 1.2;
}
//Show Pokemon stats
void Pokemon::displayStats()
{
	cout << this->name << " Stats: " << endl;
	cout << "Type: " << this->type << endl;
	cout << "Health: " << this->hp << "/" << this->baseHp << endl;
	cout << "Level: " << this->lvl << endl;
	cout << "Damage: " << this->baseDamage << endl;
	cout << "Exp: " << this->exp << "/" << this->expToNextLvl << endl << endl;
}