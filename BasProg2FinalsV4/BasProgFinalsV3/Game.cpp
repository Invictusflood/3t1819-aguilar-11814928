#include"Player.h"
#include"Pokemon.h"
#include<random>
#include<time.h>
#include<stdlib.h>

using namespace std;
//Pokemon Index
void createPokedex(vector<Pokemon*> &pokemon)
{
	//POKEMON STATS
	Pokemon* pikachu = new Pokemon("Pikachu", "Electric", 35, 25, 15, 15);
	Pokemon* bulbasaur = new Pokemon("Bulbasaur", "Grass", 45, 19, 15, 15);
	Pokemon* charmander = new Pokemon("Charmander", "Fire", 39, 22, 15, 15);
	Pokemon* squirtle = new Pokemon("Squirtle", "Water", 44, 18, 15, 15);
	Pokemon* chikorita = new Pokemon("Chikorita", "Grass", 45, 19, 15, 15);
	Pokemon* cyndaquill = new Pokemon("Cyndaquill", "Fire", 39, 22, 15, 15);
	Pokemon* totodile = new Pokemon("Totodile", "Water", 50, 35, 15, 15);
	Pokemon* treecko = new Pokemon("Treecko", "Grass", 40, 15, 15, 15);
	Pokemon* torchic = new Pokemon("Torchic", "Fire", 45, 30, 15, 15);
	Pokemon* mudkip = new Pokemon("Mudkip", "Water", 50, 40, 15, 15);
	Pokemon* turtwig = new Pokemon("Turtwig", "Grass", 55, 38, 15, 15);
	Pokemon* chimchar = new Pokemon("Chimchar", "Fire", 44, 28, 15, 15);
	Pokemon* piplup = new Pokemon("Piplup", "Fire", 53, 21, 15, 15);
	Pokemon* snivy = new Pokemon("Snivy", "Grass", 45, 25, 15, 15);
	Pokemon* tepig = new Pokemon("Tepig", "Fire", 65, 33, 15, 15);
	Pokemon* oshawott = new Pokemon("Oshawott", "Water", 55, 25, 15, 15);

	pokemon = { pikachu, bulbasaur ,charmander, squirtle, chikorita, cyndaquill,
		totodile, treecko, torchic, mudkip, turtwig, chimchar, piplup, snivy, tepig, oshawott};
}
//Player Creation and Starter Choice
void createCharacter(Player* &player, vector<Pokemon*> pokemon)
{
	string name;
	cout << "What is your name? ";
	cin >> name;
	player = new Player(name, pokemon);
}
//World Map
string determineArea(int x, int y) // Returns an area name based on the players x and y
{
	if (x >= 0 && x <= 1 && y >= 0 && y <= 1)
	{
		return "Pallet Town";
	}
	else if (x >= 0 && x <= 1 && y >= 2 && y <= 5)
	{
		return "Route 1";
	}
	else if (x >= -1 && x <= 2 && y >= 6 && y <= 9)
	{
		return "Viridian City";
	}
	else if (x >= 0 && x <= 1 && y >= 10 && y <= 13)
	{
		return "Route 2";
	}
	else if (x >= 0 && x <= 1 && y >= 14 && y <= 17)
	{
		return "Viridian Forest";
	}
	else if (x >= -1 && x <= 2 && y >= 18 && y <= 21)
	{
		return "Pewter City";
	}
	else if (x >= 3 && x <= 8 && y >= 19 && y <= 20)
	{
		return "Route 3";
	}
	else if (x >= 7 && x <= 8 && y >= 21 && y <= 22)
	{
		return "Mt. Moon";
	}
	else if (x >= 9 && x <= 14 && y >= 21 && y <= 22)
	{
		return "Route 4";
	}
	else if (x >= 15 && x <= 18 && y >= 20 && y <= 23)
	{
		return "Cerulean City";
	}
	else
	{
		return "Wall";
	}
}
//Prevents player from entering Out of Bound areas
void moveIfPossible(Player* &player, string direction, bool &didPlayerMove) //Will check if area attempting to move to is valid(has name)
{
	int moveX = 0, moveY = 0;
	bool isPossible = true;
	if (direction == "w" || direction == "W")
	{
		moveY = 1;
	}
	else if (direction == "s" || direction == "S")
	{
		moveY = -1;
	}
	else if (direction == "a" || direction == "A")
	{
		moveX = -1;
	}
	else if (direction == "d" || direction == "D")
	{
		moveX = 1;
	}
	else
	{
		cout << "Please input a valid choice" << endl;
		isPossible = false;
	}

	if (determineArea(player->x + moveX, player->y + moveY) == "Wall")
	{
		isPossible = false;
		cout << "You can't go through walls!" << endl;
	}
	else
	{
		player->moveTo(moveX, moveY);
		didPlayerMove = true;
	}
}
//Runs Game
void runGame(Player*& player, vector<Pokemon*> pokemon) //Will run when in new coordinate
{
	bool quitGame = false;
	while (quitGame == false)
	{
		string quitChoice;
		int encounterChance = 6;
		bool didPlayerMove = false , isBattleFinished = false;
		player->area = determineArea(player->x, player->y); //Finds area player is in
		player->checkAreaSafe(); //Determine if area is wildzone or not
		while (didPlayerMove == false) //Repeat until player moves
		{
			cout << "Area: " << player->area << endl;
			cout << "(" << player->x << "," << player->y << ")" << endl;
			if (isBattleFinished == false && player->isInSafeArea == false && encounterChance >= rand() % 10 + 1) //Battle if conditions are met
			{
				player->pokemonBattle(pokemon);
				isBattleFinished = true;
			}
			else
			{
				cout << "1. Move  2. Check Team";
				if (player->isInSafeArea == true) //Only include options if in safe area
				{
					cout << "  3. PokeCenter  4. PokeMart";
				}
				cout << "  0. Quit Game" << endl;
				int choice;
				cin >> choice;
				cout << endl;
				string direction;
				switch (choice)
				{
				case 1: //Move
					cout << "Where do you wanna move?" << endl;
					cout << "[W]Up - " << determineArea(player->x, player->y + 1) << "  ";
					cout << "[S]Down - " << determineArea(player->x, player->y - 1) << "  ";
					cout << "[A]Left - " << determineArea(player->x - 1, player->y) << "  ";
					cout << "[D]Right - " << determineArea(player->x + 1, player->y) << endl;
					cin >> direction;
					moveIfPossible(player, direction, didPlayerMove);
					break;
				case 2: //Check Party
					cout << "Trainer: " << player->name << endl;
					cout << "PokeDollars: P" << player->pokeDollars << endl;
					cout << "Party: " << player->party.size() << "/6" << endl;
					for (int i = 0; i < player->party.size(); i++)
					{
						player->party[i].displayStats();
					}
					break;
				case 3: //PokeCenter
					if (player->isInSafeArea == true)
					{
						player->pokeCenter();
					}
					break;
				case 4: //Pokemart
					player->pokeMart();
					break;
				case 0: //Quit Game
					cout << "Are you sure you want to quit? Progress your progress won't be saved (y/n)" << endl;
					cin >> quitChoice;
					if (quitChoice == "y" || quitChoice == "Y")
					{
						quitGame = true;
						didPlayerMove = true;
					}
					break;
				default:
					cout << "Please input one of the choices" << endl;
					break;
				}
			}
			system("pause");
			system("CLS");
		}
	}
	cout << "Thanks for playing!" << endl;
}

int main()
{
	srand(time(NULL));
	vector<Pokemon*> pokemon;
	createPokedex(pokemon);
	Player* player;
	createCharacter(player, pokemon);
	runGame(player, pokemon);
	system("pause");
	return 0;
}